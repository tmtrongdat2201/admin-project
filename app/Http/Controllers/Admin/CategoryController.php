<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        return view('admin.categories.index');
    }

    public function getList()
    {
        $categories = $this->category->getWithPagination();

        return view('admin.categories.list', compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');

    }

    public function store(CategoryRequest $request)
    {
        $this->category->create([
            'slug' => Str::slug($request->input('name'), '-'),
            'name' => $request->input('name')
        ]);

        return response()->json(['status' => 'success']);
    }

    public function edit($id)
    {
        $category = $this->category->findOrFail($id);

        return view('admin.categories.edit', compact('category'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = $this->category->findOrFail($id);
        $category->update($request->all());
    }

    public function destroy($id)
    {
        $category = $this->category->findOrFail($id);
        $category->delete($id);

        return response()->json(['status' => 'success']);
    }
}
