<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Permission;

class PermissionController extends Controller
{
    protected $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }


    public function index()
    {
        return view('admin.permissions.index');
    }


    public function getList()
    {
        $permissions = $this->permission->getWithPagination();

        return view('admin.permissions.list', compact('permissions'));
    }

    public function create()
    {
        return view('admin.permissions.create');
    }

    public function store(PermissionRequest $request)
    {
        $this->permission->create($request->all());

        return response()->json(['status' => 'success']);
    }

    public function edit($id)
    {
        $permission = $this->permission->findOrFail($id);

        return view('admin.permissions.edit', compact('permission'));
    }

    public function update(PermissionRequest $request, $id)
    {
        $permission = $this->permission->findOrFail($id);
        $permission->update($request->all());

        return response()->json(['status' => 'success']);
    }

    public function destroy($id)
    {
        $permission = $this->permission->findOrFail($id);
        $permission->delete($id);

        return response()->json(['status' => 'success']);
    }
}
