<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ImageTrait;

    protected $product;

    protected $category;

    public function __construct(Product $product, Category $category)
    {
        $this->product = $product;
        $this->category = $category;
    }

    public function index()
    {
        return view('admin.products.index');
    }

    public function getList(Request $request)
    {
        $products = $this->product->searchBy($request);

        return view('admin.products.list', compact('products'));
    }

    public function create()
    {
        $categories = $this->category->all();

        return view('admin.products.create', compact('categories'));
    }

    public function store(ProductRequest $request)
    {
        $product = $this->product->create($request->all());
        $product->syncCategories($request->categories_id);
        $patch = $this->createImage($request->file('image'));
        $product->image()->create(['url' => $patch]);;

        return response()->json(['status' => 'success']);

    }

    public function edit($id)
    {
        $product = $this->product->findOrFail($id);
        $categories = $this->category->all();

        return view('admin.products.edit', compact('product', 'categories'));
    }

    public function update(ProductRequest $request, $id)
    {
        $product = $this->product->findOrFail($id);
        $product->update($request->all());
        $product->syncCategories($request->categories_id);
        $patch = $this->createImage($request->file('image'));
        $product->image()->update(['url' => $patch]);;

        return response()->json(['status' => 'success']);

    }

    public function destroy($id)
    {
        $product = $this->product->findOrFail($id);
        $this->deleteImage($product->image->url);
        $product->image()->delete();
        $product->delete($id);

        return response()->json(['status' => 'success']);
    }

}
