<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Permission;
use App\Role;

class RoleController extends Controller
{
    protected $role, $permission;

    public function __construct(Role $role, Permission $permission)
    {
        $this->permission = $permission;
        $this->role = $role;
    }

    public function index()
    {
        return view('admin.roles.index');
    }

    public function getList()
    {
        $roles = $this->role->getWithPagination();

        return view('admin.roles.list', compact('roles'));
    }

    public function create()
    {
        $permissions = Permission::all();

        return view('admin.roles.create', compact('permissions'));
    }

    public function store(roleRequest $request)
    {
        $role = $this->role->create($request->all());
        $role->syncPermission($request->input('permissions_id'));

        return response()->json(['status' => 'success']);

    }

    public function edit($id)
    {
        $role = $this->role->findOrFail($id);
        $permissions = $this->permission->all();

        return view('admin.roles.edit', compact('role', 'permissions'));
    }

    public function update(RoleRequest $request, $id)
    {
        $role = $this->role->findOrFail($id);
        $role->update($request->all());
        $role->syncPermission($request->input('permissions_id'));

        return response()->json(['status' => 'success']);
    }

    public function destroy($id)
    {
        $role = $this->role->findOrFail($id);
        $role->delete($id);

        return response()->json(['status' => 'success']);
    }
}
