<?php

namespace App\Http\Requests;

use App\Rules\Uppercase;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:products,name,'.$this->product,
            'categories_id' => 'required',
            'image' => 'image|max:2048',
            'price' => 'required|numeric',
            'size' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute can not null',
            'unique' => ':attribute already exists',
            'numeric' => ':attribute is a number',
            'max'=>':attribute can not exceed :max characters',
            'image' => 'file is not a image',
            'image.max' => 'File can not to exceed 2048KB'
        ];
    }
}
