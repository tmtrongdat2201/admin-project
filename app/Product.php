<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'description',
        'size',
        'price',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function getWithPagination()
    {
        return $this->latest()->paginate(5);
    }

    public function syncCategories($categories_id)
    {
        return $this->categories()->sync($categories_id);
    }

    public function searchBy($data)
    {
        $name = $data['name'] ?? null;
        $price = $data['price'] ?? null;
        $category = $data['category'] ?? null;

        return $this->withName($name)->withPrice($price)->withCategory($category)->latest()->paginate(6);
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', 'LIKE', '%' . $name . '%');
    }

    public function scopeWithPrice($query, $price)
    {
        return $query->where('price', 'LIKE', '%' . $price . '%');
    }

    public function scopeWithCategory($query, $category)
    {
        return $query->whereHas('categories', fn($query) => $query
            ->where('name', 'like', '%' . $category . '%')
        );
    }
}
