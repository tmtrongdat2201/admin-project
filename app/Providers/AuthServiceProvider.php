<?php

namespace App\Providers;

use http\Client\Curl\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->define();

        Gate::before(function ($user) {
            if ($user->hasRole('admin')) {
                return true;
            }
        });

    }

    const PERMISSIONS = [
        'view_list_user', 'create_user', 'update_user', 'delete_user',
        'view_list_product', 'create_product', 'update_product', 'delete_product',
        'view_list_role', 'create_role', 'update_role', 'delete_role',
        'view_list_permission', 'create_permission', 'update_permission', 'delete_permission',
        'view_list_category', 'create_permission', 'update_category', 'delete_category',
    ];

    public function define()
    {
        foreach (self::PERMISSIONS as $permission) {
            Gate::define($permission, fn($user) => $user->hasPermission($permission));
        }
    }

}
