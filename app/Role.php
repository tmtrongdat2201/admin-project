<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name',
        'display_name',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

    public function getWithPagination()
    {
        return $this->latest()->paginate(5);
    }

    public function syncPermission($permissions_id)
    {
        return $this->permissions()->sync($permissions_id);
    }
}
