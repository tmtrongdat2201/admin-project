<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait ImageTrait
{
    protected $defaultImage = 'default.jpeg';

    protected $imagePatch = 'public/product';

    public function verifyImage($image)
    {
        return !is_null($image);
    }

    public function saveImage($image, $folder)
    {
        $fileName = time() . "_" . $image->getClientOriginalName();
        $patch = $image->storeAs($folder, $fileName);

        return Storage::url($patch);
    }

    public function createImage($image)
    {
        if ($this->verifyImage($image)) {
            return $this->saveImage($image, $this->imagePatch);
        }
        return Storage::url($this->imagePatch . '/' . $this->defaultImage);;
    }

    public function deleteImage($patch)
    {
        $fileName = basename($patch);
        if (!($fileName === $this->defaultImage)) {
            Storage::delete($this->imagePatch . '/' . $fileName);
        }
    }

    public function updateImage($image)
    {
        if ($this->verifyImage($image)) {
            return $this->saveImage($image, $this->imagePatch);
        }

    }
}
