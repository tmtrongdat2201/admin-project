$(function () {

    function getListProduct() {
        let url = $('#list-data').data('url')
        callAjax(url, 'post')
            .then(function (data) {
                $('#fetch').replaceWith(data);
            });
    }

    getListProduct();

    $(document).on('click', '.btn-show-add', function () {
        let url = $(this).data('url');
        callAjax(url)
            .then(function (data) {
                $('.modal').replaceWith(data);
                getPlugins();
                $('#modal-add').modal('show');
            })
    })

    $(document).on('click', '.btn-add', function () {
        let url = $(this).data('url');
        let formData = new FormData($('#add-form')[0]);
        callAjax(url, 'post', formData)
            .then(function () {
                $('#modal-add').modal('hide');
                getListProduct();
            })
            .catch(function (error) {
                printErrorMsg(error?.responseJSON.errors);
            });
    });

    $(document).on('click', '.btn-show-edit', function () {
        let url = $(this).data('url');
        callAjax(url)
            .then(function (data) {
                $('.modal').replaceWith(data);
                getPlugins();
                $('#modal-edit').modal('show');
            });
    })

    $(document).on('click', '.btn-edit', function () {
        let url = $(this).data('url');
        let formData = new FormData($('#edit-form')[0]);
        callAjax(url, 'post', formData)
            .then(function () {
                $('#modal-edit').modal('hide');
                getListProduct();
            })
            .catch(function (error) {
                printErrorMsg(error?.responseJSON.errors);
            });
    })

    $(document).on('click', '.btn-delete-product', function () {
        let url = $(this).data('url');
        $.confirm({
            title: 'Delete!',
            content: 'Do you want delete!',
            buttons: {
                confirm: function () {
                    callAjax(url, 'delete')
                        .then(function () {
                            getListProduct();
                        });
                },
                cancel: function () {
                },
            }
        });
    });

    $(document).on('click', '.btn-search', function () {
        let url = $(this).data('url');
        let formData = new FormData($('#search-form')[0]);
        callAjax(url, 'post', formData)
            .then(function (data) {
                $('#fetch').replaceWith(data);
            })
    })
})
