$(function () {

    getList();

    $(document).on('click', '.btn-show-add', function () {
        let url = $(this).data('url');
        callAjax(url)
            .then(function (data) {
                $('.modal').replaceWith(data);
                getPlugins();
                $('#modal-add').modal('show');
            })
    })

    $(document).on('click', '.btn-add', function () {
        let url = $(this).data('url');
        let formData = new FormData($('#add-form')[0]);
        callAjax(url, 'post', formData)
            .then(function (data) {
                $('#modal-add').modal('hide');
                getList();
            })
            .catch(function (error) {
                printErrorMsg(error.responseJSON.errors);
            });
    });

    $(document).on('click', '.btn-show-edit', function () {
        let url = $(this).data('url');
        callAjax(url)
            .then(function (data) {
                $('.modal').replaceWith(data);
                getPlugins();
                $('#modal-edit').modal('show');
            });
    })

    $(document).on('click', '.btn-edit', function () {
        let url = $(this).data('url');
        let formData = new FormData($('#edit-form')[0]);
        callAjax(url, 'post', formData)
            .then(function () {
                $('#modal-edit').modal('hide');
                getList();
            }).catch(function (error) {
            printErrorMsg(error.responseJSON.errors);
        });
    })
})
