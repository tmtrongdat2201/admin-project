$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function callAjax(url, method, data) {
    return $.ajax({
        type: method,
        url: url,
        data: data,
        processData: false,
        contentType: false,
    });
}

function getList() {
    let url = $('#list-data').data('url')
    callAjax(url)
        .then(function (data) {
            $('#fetch').replaceWith(data);
        });
}

$(document).on('click', '.btn-delete', function () {
    let url = $(this).data('url');
    $.confirm({
        title: 'Delete!',
        content: 'Do you want delete!',
        buttons: {
            confirm: function () {
                callAjax(url, 'delete')
                    .then(function () {
                        getList();
                    });
            },
            cancel: function () {
            },
        }
    });
});

function printErrorMsg(msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display', 'block');
    $.each(msg, function (key, value) {
        $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
    });
}

function getPlugins() {
    $('#select2').select2({
        placeholder: "Select a state",
        allowClear: true
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
