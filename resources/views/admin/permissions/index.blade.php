@extends('admin.layouts.master')
@section('title','Permission')
@section('content')

    <div class="bg-light justify-content-between">
        <nav class="navbar navbar-light bg-light justify-content-between">
            <button class="btn btn-outline-success btn-show-add" data-url="{{route('permission.create')}}" type="button">Add Permission</button>
        </nav>
    </div>

    <div id="list-data" data-url="{{route('permission.list')}}">
        <div id="fetch"></div>
    </div>
    <div class="modal"></div>
@endsection
@section('script')
    <script src="{{asset('assets/admin-js/web.js')}}"></script>
    <script src="{{asset('assets/admin-js/permission.js')}}"></script>
@endsection
