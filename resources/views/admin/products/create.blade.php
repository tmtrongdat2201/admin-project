
<div id="modal-add" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger print-error-msg" style="display:none">
                    <ul></ul>
                </div>
                <form id="add-form">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter...">
                        <label>Size</label>
                        <input type="text" name="size" class="form-control" placeholder="Enter...">
                        <label>price</label>
                        <input type="text" name="price" class="form-control" placeholder="Enter...">
                        <label>Description</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="2"></textarea>
                        <label>Category</label>
                        <select class="form-control" id="select2" name="categories_id[]" style="width: 100%" multiple>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}" >{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" name="image" class="form-control-file"
                               id="imgInp" onchange="readURL(this)">
                        <div class="text-center">
                            <img id="blah" alt="your image" class="rounded"
                                 src="#">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-add" data-url="{{route('product.store')}}">Add</button>
            </div>

        </div>
    </div>
</div>

