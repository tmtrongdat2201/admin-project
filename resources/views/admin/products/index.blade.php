@extends('admin.layouts.master')
@section('title','Product')
@section('content')

    <div class="bg-light justify-content-between">
        <nav class="navbar navbar-light bg-light justify-content-between">
            <button class="btn btn-outline-success btn-show-add" data-url="{{route('product.create')}}" type="button">Add
                Product
            </button>
        </nav>
    </div>
    <div class="form-group">
        <form class="form-inline" id="search-form">
            <input class="form-control mr-sm-2" name="name" type="search" placeholder="Enter name...">
            <input class="form-control mr-sm-2" name="price" type="search" placeholder="Enter price...">
            <input class="form-control mr-sm-2" name="category" type="search" placeholder="Enter category...">
            <button class="btn btn-outline-success btn-search" data-url="{{route('product.list')}}" type="button">
                Search
            </button>
        </form>
    </div>

    <div id="list-data" data-url="{{route('product.list')}}">
        <div id="fetch"></div>
    </div>
    <div class="modal"></div>
@endsection
@section('script')
    <script src="{{asset('assets/admin-js/web.js')}}"></script>
    <script src="{{asset('assets/admin-js/product.js')}}"></script>
@endsection
