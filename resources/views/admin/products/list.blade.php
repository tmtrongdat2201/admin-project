<div id="fetch" class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Products</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Size</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Size</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->name}}</td>
                        <td>{{$product->description}}</td>
                        <td>{{$product->size}}</td>
                        <td>{{$product->price}}</td>
                        <td>
                            @foreach($product->categories as $category)
                                <li>{{$category->name}}</li>
                            @endforeach
                        </td>
                        <td>
                            @if (!is_null($product->image))
                                <img src="{{ asset($product->image->url) }}" style="width: 100px; height: 100px">
                            @endif

                        </td>
                        <td>
                            <button class="btn btn-primary btn-show-edit" data-url="{{route('product.edit',$product->id)}}" data-toggle="#modal" data-target="#edit"
                                    type="button"><i class="fas fa-edit"></i></button>
                            <button class="btn btn-danger btn-delete-product" data-url="{{route('product.destroy',$product->id)}}" data-toggle="#modal" data-target="#delete"
                                    type="button"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{$products->links()}}
</div>

