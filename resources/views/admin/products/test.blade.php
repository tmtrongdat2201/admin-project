@extends('admin.layouts.master')
@section('title','Product')
@section('content')

    <div class="bg-light justify-content-between">
            <form class="form-inline" id="search-form" method="post" action="{{route('product.search')}}">
                <input class="form-control mr-sm-2" name="name" placeholder="Search">
                <button  class="btn btn-outline-success btn-search"  data-url="{{route('product.search')}}">Search</button>
            </form>
        </nav>
    </div>
    <div class="modals"></div>
@endsection
@section('script')

    <script>
        // $(document).on('click', '.btn-search', function () {
        //     let url = $(this).data('url');
        //     let formData = new FormData($('#search-form')[0]);
        //
        //     callAjax(url, 'post', formData)
        //         .then(function (data) {
        //             $('#list-data').html(data);
        //             alert('ada');
        //         })
        //
        // })
    </script>
@endsection
