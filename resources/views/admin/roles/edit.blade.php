
<div id="modal-edit" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger print-error-msg" style="display:none">
                    <ul></ul>
                </div>
                <form id="edit-form">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" value="{{$role->name}}" class="form-control" placeholder="Enter...">
                    </div>
                    <div class="form-group">
                        <label>Display name</label>
                        <input type="text" name="display_name" value="{{$role->display_name}}" class="form-control" placeholder="Enter...">
                    </div>
                    <div class="form-group">
                        <label>Permission</label>
                        <select class="form-control" id="select2" name="permissions_id[]" style="width: 100%" multiple>
                            @foreach ($permissions as $permission)
                                <option value="{{ $permission->id }}" >{{$permission->display_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary btn-edit" data-url="{{route('role.update', $role->id)}}">Edit</button>
            </div>

        </div>
    </div>
</div>

