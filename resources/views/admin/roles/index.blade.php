@extends('admin.layouts.master')
@section('title','Role')
@section('content')

    <div class="bg-light justify-content-between">
        <nav class="navbar navbar-light bg-light justify-content-between">
            <button class="btn btn-outline-success btn-show-add" data-url="{{route('role.create')}}" type="button">Add
                Role
            </button>
        </nav>
    </div>

    <div id="list-data" data-url="{{route('role.list')}}">
        <div id="fetch"></div>
    </div>
    <div class="modal"></div>
@endsection


@section('script')
        <script src="{{asset('assets/admin-js/web.js')}}"></script>
        <script src="{{asset('assets/admin-js/role.js')}}"></script>
@endsection
