<div id="fetch" class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Products</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Display Name</th>
                    <th>Permission</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Display Name</th>
                    <th>Permission</th>
                    <th>Action</th>
                </tr>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>{{$role->name}}</td>
                        <td>{{$role->display_name}}</td>
                        <td>
                            @foreach ($role->permissions as $permission)
                                <li>{{$permission->display_name}}</li>
                            @endforeach
                        </td>
                        <td>
                            <button class="btn btn-primary btn-show-edit" data-url="{{route('role.edit',$role->id)}}" data-toggle="#modal" data-target="#edit"
                                    type="button"><i class="fas fa-edit"></i></button>
                            <button class="btn btn-danger btn-delete" data-url="{{route('role.destroy',$role->id)}}" data-toggle="#modal" data-target="#delete"
                                    type="button"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{$roles->links()}}
</div>

