@extends('admin.layouts.master')
@section('title','User')
@section('content')

    <div class="bg-light justify-content-between">
        <nav class="navbar navbar-light bg-light justify-content-between">
            <button class="btn btn-outline-success btn-show-add" data-url="{{route('user.create')}}" type="button">Add User</button>
        </nav>
    </div>

    <div id="list-data" data-url="{{route('user.list')}}">
        <div id="fetch"></div>
    </div>
    <div class="modal"></div>
@endsection
@section('script')
    <script src="{{asset('assets/admin-js/web.js')}}"></script>
    <script src="{{asset('assets/admin-js/user.js')}}"></script>
@endsection
