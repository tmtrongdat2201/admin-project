<div id="fetch" class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Products</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Roles</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Roles</th>
                    <th>Action</th>
                </tr>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone}}</td>
                        <td>
                            @foreach ($user->roles as $role)
                                <li>{{$role->display_name}}</li>
                            @endforeach
                        </td>
                        <td>
                            <button class="btn btn-primary btn-show-edit" data-url="{{route('user.edit',$user->id)}}" data-toggle="#modal" data-target="#edit"
                                    type="button"><i class="fas fa-edit"></i></button>
                            <button class="btn btn-danger btn-delete" data-url="{{route('user.destroy',$user->id)}}" data-toggle="#modal" data-target="#delete"
                                    type="button"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{$users->links()}}
</div>

