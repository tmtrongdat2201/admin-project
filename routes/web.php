<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', function () {
    return view('admin.products.test');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::group(["prefix" => "admin"], function () {

        Route::group(["prefix" => "products"], function () {

            Route::get('/index', 'Admin\ProductController@index')->name('product.index')
                ->middleware('can:view_list_product');

            Route::get('/create', 'Admin\ProductController@create')->name('product.create')
                ->middleware('can:create_product');

            Route::post('/store', 'Admin\ProductController@store')->name('product.store');

            Route::post('/list', 'Admin\ProductController@getList')->name('product.list');

            Route::get('/edit/{product}', 'Admin\ProductController@edit')->name('product.edit')
                ->middleware('can:update_product');

            Route::post('/update/{product}', 'Admin\ProductController@update')->name('product.update');

            Route::delete('/delete/{product}', 'Admin\ProductController@destroy')->name('product.destroy')
                ->middleware('can:delete_product');

            Route::post('/search', 'Admin\ProductController@search')->name('product.search');
        });

        Route::group(["prefix" => "categories"], function () {

            Route::get('/index', 'Admin\CategoryController@index')->name('category.index')
                ->middleware('can:view_list_category');

            Route::get('/create', 'Admin\CategoryController@create')->name('category.create')
                ->middleware('can:create_category');

            Route::post('/store', 'Admin\CategoryController@store')->name('category.store');

            Route::get('/list', 'Admin\CategoryController@getList')->name('category.list');

            Route::get('/edit/{category}', 'Admin\CategoryController@edit')->name('category.edit')
                ->middleware('can:update_category');

            Route::post('/update/{category}', 'Admin\CategoryController@update')->name('category.update');

            Route::delete('/delete/{category}', 'Admin\CategoryController@destroy')->name('category.destroy')
                ->middleware('can:delete_category');
        });

        Route::group(["prefix" => "users"], function () {

            Route::get('/index', 'Admin\UserController@index')->name('user.index')
                ->middleware('can:view_list_user');

            Route::get('/create', 'Admin\UserController@create')->name('user.create')
                ->middleware('can:create_user');

            Route::post('/store', 'Admin\UserController@store')->name('user.store');

            Route::get('/list', 'Admin\UserController@getList')->name('user.list');

            Route::get('/edit/{user}', 'Admin\UserController@edit')->name('user.edit')
                ->middleware('can:update_user');

            Route::post('/update/{user}', 'Admin\UserController@update')->name('user.update');

            Route::delete('/delete/{user}', 'Admin\UserController@destroy')->name('user.destroy')
                ->middleware('can:delete_user');
        });

        Route::group(["prefix" => "roles"], function () {

            Route::get('/index', 'Admin\RoleController@index')->name('role.index')
                ->middleware('can:view_list_role');

            Route::get('/create', 'Admin\RoleController@create')->name('role.create')
                ->middleware('can:create_role');

            Route::post('/store', 'Admin\RoleController@store')->name('role.store');

            Route::get('/list', 'Admin\RoleController@getList')->name('role.list');


            Route::get('/edit/{role}', 'Admin\RoleController@edit')->name('role.edit')
                ->middleware('can:update_role');

            Route::post('/update/{role}', 'Admin\RoleController@update')->name('role.update');

            Route::delete('/delete/{role}', 'Admin\RoleController@destroy')->name('role.destroy')
                ->middleware('can:delete_role');
        });

        Route::group(["prefix" => "permissions "], function () {

            Route::get('/index', 'Admin\PermissionController@index')->name('permission.index')
                ->middleware('can:view_list_permission');

            Route::get('/create', 'Admin\PermissionController@create')->name('permission.create')
                ->middleware('can:create_permission');

            Route::post('/store', 'Admin\PermissionController@store')->name('permission.store');

            Route::get('/list', 'Admin\PermissionController@getList')->name('permission.list');

            Route::get('/edit/{role}', 'Admin\PermissionController@edit')->name('permission.edit')
                ->middleware('can:update_permission');

            Route::post('/update/{role}', 'Admin\PermissionController@update')->name('permission.update');

            Route::delete('/delete/{role}', 'Admin\PermissionController@destroy')->name('permission.destroy')
                ->middleware('can:delete_permission');
        });

        Route::get('/edit/{product}', 'Admin\ProductController@edit')->name('product.edit')
            ->middleware('can:update_product');

        Route::post('/update/{product}', 'Admin\ProductController@update')->name('product.update');

        Route::delete('/delete/{product}', 'Admin\ProductController@destroy')->name('product.destroy')
            ->middleware('can:delete_product');

        Route::post('/search', 'Admin\ProductController@search')->name('product.search');
    });

    Route::group(["prefix" => "categories"], function () {

        Route::get('/index', 'Admin\CategoryController@index')->name('category.index')
            ->middleware('can:view_list_category');

        Route::get('/create', 'Admin\CategoryController@create')->name('category.create')
            ->middleware('can:create_category');

        Route::post('/store', 'Admin\CategoryController@store')->name('category.store');

        Route::get('/list', 'Admin\CategoryController@getList')->name('category.list');

        Route::get('/edit/{category}', 'Admin\CategoryController@edit')->name('category.edit')
            ->middleware('can:update_category');

        Route::post('/update/{category}', 'Admin\CategoryController@update')->name('category.update');

        Route::delete('/delete/{category}', 'Admin\CategoryController@destroy')->name('category.destroy')
            ->middleware('can:delete_category');
    });

    Route::group(["prefix" => "users"], function () {

        Route::get('/index', 'Admin\UserController@index')->name('user.index')
            ->middleware('can:view_list_user');

        Route::get('/create', 'Admin\UserController@create')->name('user.create')
            ->middleware('can:create_user');

        Route::post('/store', 'Admin\UserController@store')->name('user.store');

        Route::get('/list', 'Admin\UserController@getList')->name('user.list');

        Route::get('/edit/{user}', 'Admin\UserController@edit')->name('user.edit')
            ->middleware('can:update_user');

        Route::post('/update/{user}', 'Admin\UserController@update')->name('user.update');

        Route::delete('/delete/{user}', 'Admin\UserController@destroy')->name('user.destroy')
            ->middleware('can:delete_user');
    });

    Route::group(["prefix" => "roles"], function () {

        Route::get('/index', 'Admin\RoleController@index')->name('role.index')
            ->middleware('can:view_list_role');

        Route::get('/create', 'Admin\RoleController@create')->name('role.create')
            ->middleware('can:create_role');

        Route::post('/store', 'Admin\RoleController@store')->name('role.store');

        Route::get('/list', 'Admin\RoleController@getList')->name('role.list');


        Route::get('/edit/{role}', 'Admin\RoleController@edit')->name('role.edit')
            ->middleware('can:update_role');

        Route::post('/update/{role}', 'Admin\RoleController@update')->name('role.update');

        Route::delete('/delete/{role}', 'Admin\RoleController@destroy')->name('role.destroy')
            ->middleware('can:delete_role');
    });

    Route::group(["prefix" => "permissions "], function () {

        Route::get('/index', 'Admin\PermissionController@index')->name('permission.index')
            ->middleware('can:view_list_permission');

        Route::get('/create', 'Admin\PermissionController@create')->name('permission.create')
            ->middleware('can:create_permission');

        Route::post('/store', 'Admin\PermissionController@store')->name('permission.store');

        Route::get('/list', 'Admin\PermissionController@getList')->name('permission.list');

        Route::get('/edit/{role}', 'Admin\PermissionController@edit')->name('permission.edit')
            ->middleware('can:update_permission');

        Route::post('/update/{role}', 'Admin\PermissionController@update')->name('permission.update');

        Route::delete('/delete/{role}', 'Admin\PermissionController@destroy')->name('permission.destroy')
            ->middleware('can:delete_permission');
    });

});
